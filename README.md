# Projeto Agregador de Cotações

Projeto para o desafio técnico do processo seletivo da SmarttBot.
O objetivo deste projeto é criar um sistema que agregue dados de cotações de criptomoedas em tempo real.

- [O Sistema](##o-sistema)
- [Implementação do Sistema](##implementação-do-sistema)
- [Desenvolvimento](##desenvolvimento)
- [Execução](##execução)
- [Dificuldades e Possíveis Melhorias](##dificuldades-e-possíveis-melhorias)

## O Sistema 

O sistema desenvolvido consome, em tempo real, dados de cotações de um par de moedas (*currency pair*) através de uma API, os agrega e então os armazena em um banco de dados.
Para coletar as cotações de um *currency pair*, o sistema utiliza a [API pública Poloniex](https:/docs.poloniex.com).
Os dados sobre as cotações coletados são agregados periodicamente em *candles*.
Cada *candle* é composto por quatro pontos referentes à cotação de uma moeda em um período: 
1. *open*: o preço de abertura,
2. *high*: o maior preço registrado,
3. *low*: o menor preço registrado, e
4. *close*: o preço de fechamento.

O sistema permite a definição de múltiplos períodos para a agregação dos dados. 
Isso permite, por exemplo, que *candles* com períodos de 1, 5 e 30 minutos sejam gerados simultaneamente. 
Após gerar os *candles*, o sistema os armazena em um banco de dados MySQL para possíveis consultas futuras.

## Implementação do Sistema

A implementação do sistema está contida no diretório [`aggregator-app/`](aggregator-app/), e foi feita utilizando **Python 3.6.1**.
O script [`app.py`](aggregator-app/app.py) é responsável pela execução do sistema.
Além dele, foram desenvolvidas as seguinte classes:
- [**SQLBroker**](aggregator-app/src/db_broker.py): classe abstrata para definir o comportamento de um componente que intermedia o acesso à um banco de dados SQL.
- [**MySQLBroker**](aggregator-app/src/mysql_broker.py): classe para intermediar o acesso à um banco de dados MySQL. Possui métodos para atividades como criação e remoção de tabelas, inserção de valoes, leitura de tabelas, entre outros.
- [**PoloniexReader**](aggregator-app/src/poloniex_reader.py): classe para intermediar requisições à API pública Poloniex. Dentre os métodos disponíveis, tem-se aqueles para ler os *currency pairs* disponíveis, ler últimas cotações (comando *returnTicker* da API) e obter um websocket para receber atualizações das cotações (utiliza o canal *Ticker Data*).
- [**Candle**](aggregator-app/src/candle.py): representação de um *candle*.
- [**RealTimeTickerAggregator**](aggregator-app/src/aggregator.py): componente que consome dados de cotação em tempo real, os agrega em *candles* e pode os armazenar em um banco de dados SQL.

As relações entra as classes são:
- O **MySQLBroker** herda as interfaces dos métodos de **SQLBroker**;
- O **RealTimeTickerAggregator** utiliza um **PoloniexReader** para consumir dados de cotação. Ele também utiliza objetos do tipo **Candle** para guardar os dados do período corrente. Além disso, ele utiliza, se fornecido, um **SQLBroker** para armazenar os **candles** gerados.

### Decisões de Implementação

- Optou-se por desenvolter classes para fazer o intermédio com elementos como o banco de dados e a API Poloniex para dividir as tarefas e modularizar o sistema.

- Preferiu-se definir a classe **SQLBroker** para que fosse possível, caso desejado, trocar o banco de dados SQL utilizado sem que os demais componentes do sistema fossem afetados. Assim, bastaria implementar uma nova classe herdeira de **SQLBroker** para o banco de dados escolhido.

- O método `connect` da classe **MySQLBroker** possui dois parâmetros, *tries* e *delay*, para possibilitar múltiplas tentativas de conexão. 
  Tais tentativas são necessárias principalmente para lidar com possíveis atrasos na inicialização do container do banco.

- A coleta de dados de cotação no **RealTimeTickerAggregator** foi feita utilizando o comando *returnTicker* **e** o websocket com o canal *Ticker Data* da API Poloniex. 
  Enquanto o comando é utilizado para a obteção de dados mais recentes no ínicio e no fim do período de uma *candle*, o websocket é utilizado para atualizar frequentemente os dados de **todas** as *candles* em construção.

- Quando o método `run ()`, da classe **RealTimeTickerAggregator** é invocado, ele inicializa as principais estruturas utilizadas. 
  Uma das rotinas de inicialização invocadas por ele, `__register_currency_pair`, é responsável por verificar a validade do *currency pair* informado e, caso exista um banco de dados associado, ele também cria uma tabela com o nome do *currency pair* para salvar os *candles* gerados. 
  Decidiu-se colocar a criação da tabela nesse momento para que fosse possível rodar múltiplas instâncias do agregador para diferentes *currency pairs*, embora essa funcionalidade não tenha sido testada.

- Optou-se por ler, no script `app.py`, os parâmetros de entrada e do banco de dados das variáveis de ambiente para que fosse possível recuperá-los utilizando as configurações do ambiente em execução.

- Foi utilizado um sistema de log para mostrar informações e indicar o status das operações da aplicação. Por padrão, informações de *debug* não são mostradas, mas sua exibição pode ser feita alterando-se o nível de *logging* no script `app.py`.

## Desenvolvimento

Para desenvolver o projeto, foram utilizadas ferramentas como **Docker**, **Docker Compose**, **git** e **GitLab CI**. 
Apresenta-se a seguir mais detalhes de como elas foram utilizadas.

### Ambiente de Desenvolvimento

O ambiente de desenvolvimento do sistema utiliza, como requisitado, o **Docker** e o **Docker Compose**.
Foram criadas duas imagens: uma para hospedar o banco e outra para hospedar a aplicação.
Essa abordagem foi adotada pois ela permite um melhor isolamento entre os componentes do sistema.
Assim, é possível, por exemplo, rodar múltiplas aplicações que acessam um mesmo banco.

A imagem do banco foi criada a partir da imagem *mysql*.
Escolheu-se a versão fixa da imagem *mysql:8.0* a fim de se garantir o funcionamento esperado em futuras execuções do sistema.
Abaixo segue o Dockerfile (presente no diretório `candle-db/`) utilizado para criação da imagem. 
O comando `COPY db/ /docker-entrypoint-initdb.d/` é utilizado para que seja possível realizar configurações iniciais no banco.
```
FROM mysql:8.0

COPY db/ /docker-entrypoint-initdb.d/
```

A imagem da aplicação foi criada a partir da imagem *python:3.6.1-alpine*.
Escolheu-se a imagem alpine devido ao tamanho reduzido que ela possui (88.7 MB contra 684 MB da imagem *python:3.6.1*), o que impacta diretamente no consumo de memória do sistema.
Abaixo se encontra o Dockerfile (presente no diretório `aggregator-app/`) para criação da imagem, que consiste basicamente em copiar os arquivos fonte do repositório, instalar os módulos necessários e então executar a aplicação localizada no arquivo `aggregator-app/app.py`.
```
FROM python:3.6.1-alpine

WORKDIR /app
COPY . .

RUN pip3 install -r requirements.txt

CMD [ "python3", "./app.py" ]
```

As imagens criadas são orquestradas pelo **Docker Compose**.
[O arquivo de configuração do orquestrador](docker-compose.yml) define um serviço para o banco de dados e outro para a aplicação que coleta e agrega os dados.
Ele também define as variáveis de ambiente necessárias para a criação e levantamento do banco de dados, bem como aquelas utilizadas como valor de entrada do script da aplicação.
Por último, o arquivo define uma rede para conectar os containers a serem criados.

### Git

O **git** foi utilizado durante todo o desenvolvimento do projeto.
Foram mantidos sempre dois *branches* fixos: **master** e **dev**.
Enquanto o primeiro representa o ambiente de produção, o segundo representa o ambiente de desenvolvimento.
Também foram utilizados *branches* do tipo **feature/***, para o desenvolvimento dos componentes do sistema.
Exemplos de *branches* utilizados são:
- **feature/add-broker**: utilizado para desenvolver a versão inicial do componente intermediador com o banco.
- **feature/add-aggregator**: utilizado para desenvolver o agregador de cotações.
- **feature/add-app**: utilizado para desenvolver o script de execução da aplicação.

*Branches* de **feature/*** foram sempre criadas a partir do **dev** e, após finalizados, foram mesclados ao mesmo.
Após finalizado o desenvolvimento, o **dev** foi mesclado ao **master**.


### Testes unitários e CI

Buscou-se densenvolver testes unitários para validar o comportamento esperado de cada componente do sistema.
Foram testados os componentes **MySQLBroker**, **RealTimeTickerAggregator** e **PoloniexReader**.
Os testes desenvolvidos também se encontram no diretório `aggregator-app/`, e são identificados pelo padrão **[componente]-test.py**.

Os testes unitários foram utilizado como base para a ferramenta de CI (*continuous integration*) empregada, no caso, o **GitLab CI**.
Definiu-se apenas um estágio no pipeline da CI.
Chamado de **build-and-test**, ele contém três *jobs*, um para cada teste unitário, que são executados em paralelo.
Este estágio foi definido para ser executado em todos os *branches*, visando sempre monitorar que nenhum erro fosse introduzido no sistema.

## Execução

Como requisitado, o sistema pode ser executado através do comando

```
docker-compose up
```

A execução *default* utiliza períodos de agregação de *candle* de 1, 5 e 10 minutos para o *currency pair* BTC_ETH.
É possíve definir diferentes períodos para agregação dos *candles* alterando-se a variável de ambiente `APP_PERIODS`, utilizada no serviço **agregagor**, definida no arquivo `docker-compose.yml`.
Múltiplos períodos devem ser definidos separando os valores por vírgulas.

Já o *currency pair* pode ser alterado mudando o valor da variável de ambiente `CURRENCY_PAIR`.
Os valores disponíveis são aqueles definidos na tabela *Currency Pair IDs*, disponível em https://docs.poloniex.com/#currency-pair-ids.

### Exemplo

As imagens abaixo fornecem um exemplo de uma execução do sistema.
Ao executar o comando `docker-compose up`, os containers serão levantados e o sistema entrará em operação.
Mensagens de log mostram o andamento da execução.
Toda vez que um *candle* é salvo, a aplicação imprime uma mensagem mostrando o instante de tempo da operação, os valores agregados, para qual *currency pair* e qual o período correspondente.
Exemplos dessa mensagem são mostrados a seguir.

![Exemplo de saída](imgs/output.png)

A próxima imagem mostra o resultado de uma consulta ao banco de dados, mostrando que os *candles* gerados foram inseridos na tabela devida.

![Select no banco](imgs/db_select.png)

## Dificuldades e Possíveis Melhorias

- Minha principal dificuldade foi no desenvolvimento dos testes unitários para a ferramenta de CI.
Os *jobs* definidos são simples e não consegui pensar em uma maneira de avaliar automaticamente o comportamento do sistema.
Assim, os testes precisam ser melhorados para serem mais eficientes.

- O tratamento de exceções do sistema em geral também pode ser melhorado.
Tive algumas dúvidas durante o desenvolvimento quanto à melhor forma de demonstrar erros, sendo através de retornos de funções ou levantando exceções.

- Os arquivos do banco de dados poderiam ser mapeados para um volume externo para que fosse possível criar cópias e utilizar ferramentas de backup. Essa funcionalidade não foi adicionada pois não era o foco do projeto.