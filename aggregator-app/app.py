from src.mysql_broker import MySQLBroker
from src.aggregator import RealTimeTickerAggregator
import logging
import os

logging.basicConfig (level=logging.INFO)
logger = logging.getLogger ("App")

if __name__ == "__main__":
    try:
        dbInfo = {
            "host": os.environ["DB_HOST"],
            "port": os.environ["DB_PORT"],
            "user": os.environ["MYSQL_USER"],
            "password": os.environ["MYSQL_PASSWORD"],
            "database": os.environ["MYSQL_DATABASE"]
        }
        periods = [int (p) for p in os.environ["APP_PERIODS"].split (',')]
        currencyPair = os.environ["CURRENCY_PAIR"]
    except KeyError as err:
        logger.error ("Missing env variables: {}".format (err))
        exit (1)
    logger.info ("will run with periods={} for currencyPair={}".format (
        periods, 
        currencyPair
    ))
    # create db broker to add to aggregator
    logger.info ("Initiating db broker")
    broker = MySQLBroker ()
    broker.connect (dbInfo, tries=30, delay=5)
    assert (broker.is_connected ()), "Broker could not connect to the MySQL "\
                                     "database."
    # create aggregator and start it
    try:
        logger.info ("Initiating aggregator")
        aggregator = RealTimeTickerAggregator ()
        aggregator.set_db_broker (broker)
        aggregator.run (periods, currencyPair)
    except Exception as err:
        logger.error ("Failed to run aggregator: {}".format (err))
