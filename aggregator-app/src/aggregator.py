from .poloniex_reader import PoloniexReader
from .db_broker import SQLBroker
from .candle import Candle
from datetime import datetime
from threading import Timer, Lock
import websocket
import logging
import json

logger = logging.getLogger ("RealTimeTickerAggregator")

class RealTimeTickerAggregator:
    """
    Class for a real time ticker aggregator. It consumes the poloniex API to 
    create periodic candles and may store them into a database.
    """
    def __init__ (self):
        self.dbInfo   = None
        self.dbBroker = None
        self.candles = {}
        self.candleLocks = {}
        self.periods = [] # periods in seconds
        self.currencyPair = None
        self.poloniex = PoloniexReader ()

    def __register_candle_period (self, period):
        """
        Intern method to register a period. It creates the candle structure and 
        a lock to avoid race conditions while tryinto to write on it.
        """
        assert (period > 0), "Period must be > 0."
        if not (period in self.periods):
            logger.debug ("Adding candle for period={}min.".format (period))
            self.periods.append (period)
            self.candles[period] = Candle (
                open=None,
                high=None,
                low=None,
                close=None
            )
            self.candleLocks[period] = Lock ()
        else:
            logger.debug ("Duplicated candle for period={}min.".format (period))

    def __register_currency_pair (self, currencyPair):
        """
        Intern method to save the currency pair and get its id. If a database 
        broker was provided, it also creates a table to store future candles.
        """
        currencyPairs = self.poloniex.get_currency_pairs ()
        for cpair in currencyPairs:
            if cpair["name"] == currencyPair:
                self.currencyPair = cpair
                break
        assert (self.currencyPair), "Unknown currency pair: " + currencyPair
        if self.dbBroker:
            logger.debug ("Creating table curr.pair={}".format (currencyPair))
            tableCreated = self.dbBroker.create_new_table (
                currencyPair, 
                "id INT AUTO_INCREMENT PRIMARY KEY,"
                "period INT NOT NULL,"
                "datetime DATETIME NOT NULL,"
                "open FLOAT NOT NULL,"
                "high FLOAT NOT NULL,"
                "low FLOAT NOT NULL,"
                "close FLOAT NOT NULL"
            )
            logger.debug ("Success")
            assert (tableCreated), "Could not create table " + currencyPair
            self.dbBroker.set_insert_table_pattern (
                currencyPair,
                "period,datetime,open,high,low,close"
            )

    def set_db_broker (self, broker):
        """
        Set a database broker to store candles.
        """
        assert (isinstance (broker, SQLBroker)), "broker must be of type SQLBroker."
        assert (broker.is_connected ()), "Database is not connected."
        self.dbBroker = broker
        logger.info ("Added a database broker")

    def run (self, periods=[], currencyPair=None):
        """
        Run the aggregator for a currencyPair. Tickers are aggregated into 
        candels with periods (in min.) inserted in the periods list.
        """
        assert (len (periods) > 0), "Requires at least 1 candle period."
        assert (currencyPair), "Missing the currency pair."
        for period in periods:
            self.__register_candle_period (period)
        self.__register_currency_pair (currencyPair)
        logger.info ("Collecting for {} in periods={}".format (
            currencyPair, 
            periods)
        )
        self.__start_collecting ()

    def __start_collecting (self):
        """
        Start the data collection about the tickers.
        """
        # initiate candles and then set timers to finish them
        self.__start_candles ()
        for period in self.periods:
            t = Timer (period * 60, self.__finish_candle, [period])
            t.start ()
        # loop to update all candles
        self.__update_loop ()

    def __update_loop (self):
        """
        'Infinite' loop that uses the poloniex tickers update websocket to 
        update candle values.
        """
        ws = self.poloniex.get_tickers_update_ws ()
        for rawUpdate in ws:
            updates = json.loads (rawUpdate)[2:]
            for update in updates:
                if update[0] == self.currencyPair["id"]:
                    lastPrice = update[1]
                    # check high and low for each candle
                    for period in self.periods:
                        with self.candleLocks[period]:
                            if self.candles[period].high < lastPrice:
                                self.candles[period].high = lastPrice
                            if self.candles[period].low > lastPrice:
                                self.candles[period].low = lastPrice
                    break

    def __start_candles (self):
        """
        Initiate the candles for all periods using a first read.
        """
        logger.debug ("Starting candles")
        ticker = self.poloniex.get_ticker_for (self.currencyPair["name"])
        logger.debug ("Got ticker.")
        # ticker = {id, last, lowestAsk, highestBid, percentChange, baseVolume, 
        # quoteVolume, isFrozen, high24hr, low24hr}
        if ticker:
            for period in self.candles.keys ():
                self.candles[period].open = ticker["last"]
                self.candles[period].high = ticker["last"]
                self.candles[period].low  = ticker["last"]
        else:
            raise Exception ("Could not get ticker.")
    
    def __save_candle_from_period (self, period):
        """
        If a database broker was added, it is used to store the candle in a 
        table.
        """
        dt = datetime.now ()
        values = (
            period,
            dt,
            self.candles[period].open,
            self.candles[period].high,
            self.candles[period].low,
            self.candles[period].close
        )
        logger.info ("At {}: saving candle {} for {} period={}min.".format (
            dt,
            self.candles[period],
            self.currencyPair["name"],
            period
        ))
        if self.dbBroker:
            res = self.dbBroker.insert_values (
                self.currencyPair["name"], 
                values
            )
            assert (res), "Failed to store candle in DB"

    def __finish_candle (self, period):
        """
        Finish the candle for the current period and prepare it to the next.
        """
        # last read in the period
        ticker = self.poloniex.get_ticker_for (self.currencyPair["name"])
        if ticker:
            # update and save the current candle
            with self.candleLocks[period]:
                self.candles[period].close = ticker["last"]
                if self.candles[period].high < ticker["last"]:
                    self.candles[period].high = ticker["last"]
                if self.candles[period].low > ticker["last"]:
                    self.candles[period].low = ticker["last"]
                logger.info ("Finished candle for "
                             "period={}min.".format (period))
                self.__save_candle_from_period (period)
                logger.info ("Started new candle for "
                             "period={}min.".format (period))
                # update for the next period
                self.candles[period].open = ticker["last"]
                self.candles[period].high = ticker["last"]
                self.candles[period].low  = ticker["last"]
            # program call to next period
            t = Timer (period * 60, self.__finish_candle, [period])
            t.start ()
        else:
            raise Exception ("Could not get ticker.")



