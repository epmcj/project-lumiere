from .db_broker import SQLBroker
import mysql.connector
import logging
import time

logger = logging.getLogger ("MySQLBroker")

class MySQLBroker (SQLBroker):
    """
    Broker for a MySQL database.
    """
    def __init__ (self):
        self.dbInfo = {
            "host": None,
            "port": None,
            "user": None,
            "password": None,
            "name": None
        }
        self.db = None
        self.isConn = False
        self.delayDefault  = 5.0 # in seconds
        self.triesDefault = 10
        self.insertCmds = {}

    def is_connected (self):
        """
        Return true if the broker is connected to a database, and False 
        otherwise.
        """
        return (self.db != None)

    def set_insert_table_pattern (self, table, valuesNames):
        """
        Define the insertion pattern to be used when trying to insert values to
        a table. 
        Return the generated pattern.
        TODO: parameter validation
        """
        columns = valuesNames.count (",") 
        cmd = "INSERT INTO " + table + " (" + valuesNames + ") VALUES ("
        for i in range (columns):
            cmd += "%s, "
        cmd += "%s)"
        self.insertCmds[table] = cmd
        logger.info ("Insert patter created: " + cmd)
        return self.insertCmds[table]


    def connect (self, dbInfo, tries=None, delay=None):
        """
        Connect the broker to the database. 
        dbInfo must be a dict with fields: 'host', 'port', 'user', 'password', 
        and 'database'.
        """
        assert (dbInfo["host"]), "Missing host information"
        assert (dbInfo["port"]), "Missing port information"
        assert (dbInfo["user"]), "Missing user information"
        assert (dbInfo["password"]), "Missing password information"
        assert (dbInfo["database"]), "Missing database"
        self.dbInfo = {
            "host": dbInfo["host"],
            "port": dbInfo["port"],
            "user": dbInfo["user"],
            "password": dbInfo["password"],
            "database": dbInfo["database"]
        }
        if not tries:
            tries = self.triesDefault
        if not delay:
            delay = self.delayDefault
        logger.debug ("broker: (tries={},delay={})".format (tries, delay))
        while True: 
            try:
                logger.debug ("Trying to connect to db "
                               "with {}".format (dbInfo))
                self.db = mysql.connector.connect (
                    host=self.dbInfo["host"],
                    port=self.dbInfo["port"],
                    user=self.dbInfo["user"],
                    password=self.dbInfo["password"],
                    database=self.dbInfo["database"]
                )
                break
            except mysql.connector.Error as err:
                if err.errno == 2003: #2013
                    logger.info ("Could not connect to the database.")
                    tries -= 1
                    if tries:
                        logger.info ("\twill try again in {}s".format (delay))
                        time.sleep (delay)
                    else:
                        logger.error ("Failed to connect to the database: "
                                       "reached max tries.")
                        return False
                else:
                    logger.error ("Failed to connect to the database: "
                                   "{}".format (err))
                    return False
            except Exception as err:
                logger.error ("Failed to connect to the database: "
                               "{}".format (err))
                return False
        logger.info ("Connected to " + dbInfo["database"])
        return True

    def close_connection (self):
        """
        Close the connection between the broker and the database.
        """
        if self.is_connected ():
            self.db.close ()
            logger.info ("Closed connection")
            self.db = None
        else:
            logger.warning ("Can not close connection because "
                             "it is not connected")

    def create_new_table (self, table, columnsDefinition):
        """
        Create a new table IF a table with same name does not exists. Parameter
        columnsDefinition must follow pattern "<column1_name> <...> <datatype1> 
        <...>, [...], <columnN_name> <...> <datatypeN> <...>". 
        """
        assert (len (table) > 0), "Missing table name."
        assert (len (columnsDefinition) > 0), "Missing columns definition."
        assert (self.is_connected ()), "Missing database connection."
        try:
            cursor = self.db.cursor ()
            cmd = "CREATE TABLE IF NOT EXISTS {} ({})".format (
                table, 
                columnsDefinition
            )
            logger.debug ("Will execute: " + cmd)
            cursor.execute (cmd)
            logger.info ("Created new table: " + table)
            cursor.close ()
            return True
        except mysql.connector.errors.OperationalError:
            logger.error ("Connection to the database was lost")
            self.db = None
            return False
        except mysql.connector.Error as err:
            logger.error ("Failed to create table: {}".format (err.msg))
            return False

    def remove_table (self, table):
        """
        Remove a table from the database (using DROP command).
        """
        assert (self.is_connected ()), "Missing database connection."
        try:
            cursor = self.db.cursor ()
            cmd = "DROP TABLE IF EXISTS {}".format (table)
            logger.debug ("Will execute: " + cmd)
            cursor.execute (cmd)
            logger.info ("Removed table: " + table)
            cursor.close ()
        except mysql.connector.errors.OperationalError:
            logger.error ("Connection to the database was lost")
            self.db = None

    def get_tables (self):
        """
        Get tables in the database.
        """
        assert (self.is_connected ()), "Missing database connection."
        try:
            cursor = self.db.cursor ()
            logger.debug ("Will execute: SHOW TABLES")
            cursor.execute ("SHOW TABLES")
            tables = [table[0] for table in cursor.fetchall ()]
            cursor.close ()
            return tables
        except mysql.connector.errors.OperationalError:
            logger.error ("Connection to the database was lost")
            self.db = None

    def insert_values (self, table, values):
        """
        Insert values into a table. The method uses the insertion pattern 
        defined using set_insert_table_pattern method. 
        """
        assert (self.is_connected ()), "Missing database connection."
        assert (self.insertCmds[table]), "Missing insertion command."
        try:
            cursor = self.db.cursor ()
            cmd = self.insertCmds[table]
            logger.debug ("Will execute: "
                           "{} % {}".format (cmd, values))
            cursor.execute (cmd, values)
            self.db.commit ()
            logger.info ("Success")
            return True
        except mysql.connector.errors.OperationalError:
            logger.error ("Connection to the database was lost")
            self.db = None
            return False
        # except mysql.connector.errors.IntegrityError:
        except (mysql.connector.errors.Error, Exception) as err:
            logger.error ("Failed to insert values {}: {}".format (
                values, err)
            )
            return False
        finally:
            cursor.close ()

    def select_all_from_table (self, table):
        """
        Get all rows in a table using the 'SELECT * FROM <table>' command.
        On success, returns a list with the table rows.
        """
        assert (self.is_connected ()), "Missing database connection."
        try:
            cursor = self.db.cursor ()
            cmd = "SELECT * FROM {}".format (table)
            logger.debug ("Will execute: " + cmd)
            cursor.execute (cmd)
            rows = cursor.fetchall ()
            cursor.close ()
            logger.info ("Success on select all")
            return rows
        except mysql.connector.errors.OperationalError:
            logger.error ("Connection to the database was lost")
            self.db = None
            return None
        except mysql.connector.Error as err:
            logger.error ("Failed to select all rows: {}".format (err.msg))
            return None

    def select_from_table_where (self, table, condition):
        """
        Get all rows that fit the condition in a table using the 'SELECT * FROM 
        <table> WHERE <condition>' command.
        On success, returns a list with the table rows.
        """
        assert (self.is_connected ()), "Missing database connection."
        try:
            cursor = self.db.cursor ()
            cmd = "SELECT * FROM {} WHERE {}".format (table, condition)
            logger.debug ("Will execute: " + cmd)
            cursor.execute (cmd)
            rows = cursor.fetchall ()
            cursor.close ()
            logger.info ("Success on select with condition")
            return rows
        except mysql.connector.errors.OperationalError:
            logger.error ("Connection to the database was lost")
            self.db = None
            return None
        except mysql.connector.Error as err:
            logger.error ("Failed to select rows: {}".format (err.msg))
            return None
