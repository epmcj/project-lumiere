import websocket
import requests
import logging
import json
import sys

TD_CHANNEL = 1002
logger = logging.getLogger ("PoloniexReader")

class PoloniexReader:
    """
    Poloniex API Reader for public requests. 
    Reference: https://docs.poloniex.com/
    """
    def __init__ (self):
        pass

    def get_currencies (self):
        """
        Get list of available currencies.
        On success, returns a list of pairs (id, name). 
        On failure, returns value None.
        """
        try:
            res = requests.get ("https://poloniex.com/public?"
                                "command=returnCurrencies")
            currencies = json.loads (res.text)
            logger.info ("Got currencies")
            return [ 
                (curr, currencies[curr]["name"]) for curr in currencies.keys () 
            ]
        except json.JSONDecodeError:
            logger.error ("Failed to get currencies: " + 
                           res.text)
            return None
        except requests.RequestException as err:
            logger.error ("Failed to get currencies: {}".format(err))
            return None
        
    def get_currency_pairs (self):
        """
        Get list of available currency pairs.
        On success, returns a list of pairs (name, id). 
        On failure, returns value None.
        """
        try:
            res = requests.get ("https://poloniex.com/public?"
                                "command=returnTicker")
            tickers = json.loads (res.text)
            logger.info ("Got currency pairs")
            cpairs = []
            for cpair in tickers.keys ():
                cpairs.append ({"name": cpair, "id":tickers[cpair]["id"]})
            return cpairs
        except json.JSONDecodeError:
            logger.error ("Failed to get currency pairs: " + 
                            res.text)
            return None
        except requests.RequestException as err:
            logger.error ("Failed to get currency pairs: {}".format(err))
            return None
        

    def get_tickers (self):
        """
        Get current ticker for each currency pair using command returnTicker. 
        On success, returns a dictionary with pairs (currency_pair, 
        information), where information = {id, last, lowestAsk, highestBid, 
        percentChange, baseVolume, quoteVolume, isFrozen, high24hr, low24hr}.
        On failure, returns value None.
        """
        try:
            res = requests.get ("https://poloniex.com/public?"
                                "command=returnTicker")
            logger.info ("Got tickers")
            return json.loads (res.text)
        except json.JSONDecodeError:
            logger.error ("Failed to get tickers: " + res.text)
            return None
        except requests.RequestException as err:
            logger.error ("Failed to get tickers: {}".format(err))
            return None

    def get_ticker_for (self, currencyPair):
        """
        Get current ticker for the currency pair using command returnTicker.
        On success, returns the ticker for the requested currency pair. 
        On failure, returns value None.
        """
        tickers = self.get_tickers ()
        if not tickers:
            return None
        try:
            ticker = tickers[currencyPair]
            logger.info ("Got ticker for " + currencyPair)
            return ticker
        except KeyError:
            logger.error ("Currency pair " + currencyPair + " not found")
            return None

        
    def get_tickers_update_ws (self):
        """
        Get a websocket that receives ticker updates (using channel Ticker 
        Data).
        On success, returns the websocket.
        On failure, return value None.
        """
        try:
            url = "wss://api2.poloniex.com"
            ws = websocket.create_connection (url)
            # Subscribe to Ticker Data channel.
            req = {
                "command": "subscribe",
                "channel": TD_CHANNEL
            }
            ws.send (json.dumps (req))
            res = ws.recv ()
            if res == "[{},1]".format (1002):
                return ws
            logger.error ("Got bad answer: " + res)
            return None
        except Exception as err:
            logger.error ("Failed to connect to "
                           "update channel: {}".format (err))
            return None