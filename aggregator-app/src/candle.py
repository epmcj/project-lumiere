class Candle:
    """
    Container class to hold candle data.
    """
    def __init__ (self, open, high, low, close):
        self.open  = open
        self.high  = high
        self.low   = low
        self.close = close

    def __str__ (self):
        return "[open:{}, high:{}, low:{}, close:{}]".format (
            self.open,
            self.high,
            self.low,
            self.close
        )
