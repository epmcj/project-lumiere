class SQLBroker:
    """
    Base class for a broker to SQL databases.
    """
    def is_connected (self):
        """
        Return True if the broker is connected to the database, and False 
        otherwise.
        """
        raise NotImplementedError

    def connect (self, dbInfo, tries, delay):
        """
        Connect the broker with a database using the database info (dbInfo).
        tries is number of attempts before giving up and delay is the delay
        between each connection attempt.
        """
        raise NotImplementedError

    def close_connect (self):
        """
        Close the currenct connection.
        """
        raise NotImplementedError

    def create_new_table (self, table, columnsDefinition):
        """
        Create a new table in the database.
        On success, returns True.
        On failure, returns False.
        """
        raise NotImplementedError

    def remove_table (self, table):
        """
        Remove (drop) table in the database.
        """
        raise NotImplementedError

    def get_tables (self):
        """
        Get tables in the database as a list.
        """
        raise NotImplementedError

    def set_insert_table_pattern (self, table, valuesNames):
        """
        Define a insertion pattern to be used when inserting values to a table.
        valueNames must be of format "<value1>,<value2>,[...],<valueN>".
        """
        raise NotImplementedError

    def insert_values (self, table, values):
        """
        Insert values in a table.
        On success, returns True.
        On failure, returns False.
        """
        raise NotImplementedError

    def select_all_from_table (self, table):
        """
        Return all rows from a table.
        On success, returns a list with the rows.
        On failure, returns None.        
        """
        raise NotImplementedError

    def select_from_table_where (self, table, condition):
        """
        Return all the rows from a table that match the condition.
        On success, returns a list with the rows.
        On failure, returns None.
        """
        raise NotImplementedError