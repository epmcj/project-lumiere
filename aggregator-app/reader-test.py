import websocket
from src.poloniex_reader import PoloniexReader

def test_get_currencies ():
    """
    Test whether the reader is able to get the list of currencies or not.
    """
    reader = PoloniexReader ()
    assert (reader.get_currencies ())

def test_get_currency_pairs ():
    """
    Test whether the reader is able to get the list of currency pairs or not.
    """
    reader = PoloniexReader ()
    assert (reader.get_currency_pairs ())

def test_get_tickers ():
    """
    Test whether the reader is able to get current tickers or not.
    """
    reader = PoloniexReader ()
    assert (reader.get_tickers ())

def test_get_ticker_for_listed_currency ():
    """
    Test whether the reader is able to get the tickers for a listed currency or 
    not.
    """
    reader = PoloniexReader ()
    assert (reader.get_ticker_for ('BTC_BCN'))

def test_get_ticker_for_unlisted_currency ():
    """
    Test whether the reader returns None when a unlisted currency is requested.
    """
    reader = PoloniexReader ()
    assert (reader.get_ticker_for ('Poloniex') == None)


def test_get_ticker_update_ws ():
    """
    Test whether the reader is able to return a websocket for ticker updates or 
    not.
    """
    reader = PoloniexReader ()
    ws = reader.get_tickers_update_ws ()
    assert (ws)
    ws.close ()

def test_get_updates_from_ticker_update_ws ():
    """
    Test whether the websocket returned is able to receive updates or not.
    """
    reader = PoloniexReader ()
    ws = reader.get_tickers_update_ws ()
    assert (ws.recv ())
    ws.close ()

if __name__ == "__main__":
    test_get_currencies ()
    test_get_tickers ()
    test_get_ticker_for_listed_currency ()
    test_get_ticker_for_unlisted_currency ()
    test_get_ticker_update_ws ()
    test_get_updates_from_ticker_update_ws ()