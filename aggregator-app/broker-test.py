from src.mysql_broker import MySQLBroker
import mysql.connector
import os
import time

try:
    dbInfo = {
        "host": os.environ["MYSQL_HOST"],
        "port": os.environ["MYSQL_PORT"],
        "user": os.environ["MYSQL_USER"],
        "password": os.environ["MYSQL_PASSWORD"],
        "database": os.environ["MYSQL_DATABASE"]
    }
except KeyError as err:
    print ("Missing env variables: {}".format (err))
    exit (1)

def test_connect_without_db_info ():
    broker = MySQLBroker ()
    try:
        broker.connect ({}, tries=1)
        exit (1)
    except Exception:
        pass

def test_connect_bad_host ():
    broker = MySQLBroker ()
    badDBInfo = {
        "host": dbInfo["host"][::-1],
        "port": dbInfo["port"],
        "user": dbInfo["user"],
        "password": dbInfo["password"],
        "database": dbInfo["database"]
    }
    assert (not broker.connect (badDBInfo, tries=1))

def test_connect_bad_port ():
    broker = MySQLBroker ()
    badDBInfo = {
        "host": dbInfo["host"],
        "port": str (int (dbInfo["port"]) + 1),
        "user": dbInfo["user"],
        "password": dbInfo["password"],
        "database": dbInfo["database"]
    }
    assert (not broker.connect (badDBInfo, tries=1))

def test_connect_bad_user ():
    broker = MySQLBroker ()
    badDBInfo = {
        "host": dbInfo["host"],
        "port": dbInfo["port"],
        "user": dbInfo["user"][::-1],
        "password": dbInfo["password"],
        "database": dbInfo["database"]
    }
    assert (not broker.connect (badDBInfo, tries=1))

def test_connect_bad_password ():
    broker = MySQLBroker ()
    badDBInfo = {
        "host": dbInfo["host"],
        "port": dbInfo["port"],
        "user": dbInfo["user"],
        "password": dbInfo["password"][::-1],
        "database": dbInfo["database"]
    }
    assert (not broker.connect (badDBInfo, tries=1))

def test_connect_bad_database ():
    broker = MySQLBroker ()
    badDBInfo = {
        "host": dbInfo["host"],
        "port": dbInfo["port"],
        "user": dbInfo["user"],
        "password": dbInfo["password"],
        "database": dbInfo["database"][::-1]
    }
    assert (not broker.connect (badDBInfo, tries=1))

def test_connect_database_ok ():
    broker = MySQLBroker ()
    assert (broker.connect (dbInfo))

def test_create_table ():
    broker = MySQLBroker ()
    broker.connect (dbInfo)
    tableName = "testcreatetable"
    tableDef = "id INT PRIMARY KEY, bla INT"
    broker.create_new_table (tableName, tableDef)

    db = mysql.connector.connect (
        host=dbInfo["host"],
        port=dbInfo["port"],
        user=dbInfo["user"],
        password=dbInfo["password"],
        database=dbInfo["database"]
    )
    cursor = db.cursor ()
    cursor.execute ("SHOW TABLES")
    tables = [table[0] for table in cursor.fetchall ()]
    assert (tableName in tables)
    cursor.close ()
    db.close ()
    broker.close_connection ()

def test_remove_table ():
    broker = MySQLBroker ()
    broker.connect (dbInfo)
    tableName = "testcreatetable"
    tableDef = "id INT PRIMARY KEY, bla INT"
    broker.create_new_table (tableName, tableDef)
    broker.remove_table (tableName)

    db = mysql.connector.connect (
        host=dbInfo["host"],
        port=dbInfo["port"],
        user=dbInfo["user"],
        password=dbInfo["password"],
        database=dbInfo["database"]
    )
    cursor = db.cursor ()
    cursor.execute ("SHOW TABLES")
    tables = [table[0] for table in cursor.fetchall ()]
    assert (not tableName in tables)
    cursor.close ()
    db.close ()
    broker.close_connection ()

def test_get_tables ():
    broker = MySQLBroker ()
    broker.connect (dbInfo)
    tableName = "testcreatetable"
    tableDef = "id INT PRIMARY KEY, bla INT"
    broker.create_new_table (tableName, tableDef)

    db = mysql.connector.connect (
        host=dbInfo["host"],
        port=dbInfo["port"],
        user=dbInfo["user"],
        password=dbInfo["password"],
        database=dbInfo["database"]
    )
    cursor = db.cursor ()
    cursor.execute ("SHOW TABLES")
    btables = broker.get_tables ()

    tables = [table[0] for table in cursor.fetchall ()]
    assert (btables == tables)
    cursor.close ()
    db.close ()
    broker.close_connection ()

def test_insert_table ():
    broker = MySQLBroker ()
    broker.connect (dbInfo)
    tableName = "testinserttable"
    tableDef = "id INT PRIMARY KEY, bla INT"
    broker.remove_table (tableName)
    broker.create_new_table (tableName, tableDef)

    db = mysql.connector.connect (
        host=dbInfo["host"],
        port=dbInfo["port"],
        user=dbInfo["user"],
        password=dbInfo["password"],
        database=dbInfo["database"]
    )
    cursor = db.cursor ()
    cursor.execute("select * from " + tableName)
    cursor.fetchall ()
    before = cursor.rowcount
    cursor.close ()
    db.close ()

    broker.set_insert_table_pattern (tableName, "id, bla")
    assert (broker.insert_values (tableName, (123, 2)))

    db = mysql.connector.connect (
        host=dbInfo["host"],
        port=dbInfo["port"],
        user=dbInfo["user"],
        password=dbInfo["password"],
        database=dbInfo["database"]
    )

    cursor = db.cursor ()
    cursor.execute("select * from " + tableName)
    cursor.fetchall ()
    assert (cursor.rowcount == (before + 1))
    cursor.close ()
    db.close ()
    broker.remove_table (tableName)
    broker.close_connection ()

def test_select_all_from_empty_table ():
    broker = MySQLBroker ()
    broker.connect (dbInfo)
    tableName = "testsemptytable"
    tableDef = "id INT PRIMARY KEY, bla INT"
    broker.create_new_table (tableName, tableDef)
    assert (len (broker.select_all_from_table (tableName)) == 0)
    broker.remove_table (tableName)
    broker.close_connection ()

def test_select_all_from_not_empty_table ():
    broker = MySQLBroker ()
    broker.connect (dbInfo)
    tableName = "testsnotemptytable"
    tableDef = "id INT PRIMARY KEY, bla INT"
    broker.create_new_table (tableName, tableDef)

    broker.set_insert_table_pattern (tableName, "id, bla")
    broker.insert_values (tableName, (123, 2))

    assert (len (broker.select_all_from_table (tableName)) != 0)
    broker.remove_table (tableName)
    broker.close_connection ()

def test_select_where_from_empty_table ():
    broker = MySQLBroker ()
    broker.connect (dbInfo)
    tableName = "testswheretable"
    tableDef = "id INT PRIMARY KEY, bla INT"
    broker.create_new_table (tableName, tableDef)

    broker.set_insert_table_pattern (tableName, "id, bla")
    broker.insert_values (tableName, (123, 2))

    assert (len (broker.select_from_table_where (tableName, "id=123")) != 0)
    broker.remove_table (tableName)
    broker.close_connection ()


if __name__ == "__main__":
    test_connect_without_db_info ()
    test_connect_bad_host ()
    test_connect_bad_port ()
    test_connect_bad_user ()
    test_connect_bad_password ()
    test_connect_bad_database ()
    test_connect_database_ok ()
    test_create_table ()
    test_remove_table ()
    test_get_tables ()
    test_insert_table ()
    test_select_all_from_empty_table ()
    test_select_all_from_not_empty_table ()
    test_select_where_from_empty_table ()