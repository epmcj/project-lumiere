from src.aggregator import RealTimeTickerAggregator
from src.mysql_broker import MySQLBroker

def test_run_with_empyt_periods ():
    try:
        agg = RealTimeTickerAggregator ()
        agg.run (currencyPair="BTC_ETH")
    except Exception:
        pass
    else:
        raise Exception ()

def test_run_with_negative_period ():
    try:
        agg = RealTimeTickerAggregator ()
        agg.run (periods=[-1], currencyPair="BTC_ETH")
    except Exception:
        pass
    else:
        raise Exception ()

def test_run_with_period_equal_zero ():
    try:
        agg = RealTimeTickerAggregator ()
        agg.run (periods=[0], currencyPair="BTC_ETH")
    except Exception:
        pass
    else:
        raise Exception ()

def test_run_with_none_currency_pair ():
    try:
        agg = RealTimeTickerAggregator ()
        agg.run (periods=[1])
    except Exception:
        pass
    else:
        raise Exception ()

def test_run_with_unknown_currency_pair ():
    try:
        agg = RealTimeTickerAggregator ()
        agg.run ([1], "unknown")
    except Exception:
        pass
    else:
        raise Exception ()

def test_set_bad_db_broker ():
    try:
        agg = RealTimeTickerAggregator ()
        agg.set_db_broker (None)
    except Exception:
        pass
    else:
        raise Exception ()

def test_set_disconnected_db_broker ():
    try:
        agg = RealTimeTickerAggregator ()
        agg.set_db_broker (MySQLBroker ())
    except Exception:
        pass
    else:
        raise Exception ()

if __name__ == "__main__":
    test_run_with_empyt_periods ()
    test_run_with_negative_period ()
    test_run_with_period_equal_zero ()
    test_run_with_none_currency_pair ()
    test_run_with_unknown_currency_pair ()
    test_set_bad_db_broker ()
    test_set_disconnected_db_broker ()